from datetime import date

def inputIsNotEmpty(input):
    if len(input) > 0:
        return True
    return False

def getActualYear():
    today = date.today()
    return str(today.year)[2] + str(today.year)[3]

def getActualMonth():
    today = date.today()
    return str(today.month)


def verifyDate(month, year):
    
    if inputIsNotEmpty(month) and inputIsNotEmpty(year):
        if len(str(month)) == 2 and len(str(year)) == 2:
            if month.isdigit() and year.isdigit():
                if int(month) <= 12 and int(month) >= 1:
                    if (int(getActualYear()) == int(year) and int(getActualMonth()) <= int(month)) or (int(getActualYear()) < int(year)):
                        if (int(getActualYear()) + 4) >= int(year):
                            return True
    return False

def verifyCVV(cvv):
    if inputIsNotEmpty(cvv) == False:
        return False
    elif len(cvv) != 3:
        return False
    elif cvv.isdigit() == False:
        return False
    else:
        return True

def verifyCardNumber(numCard):
    if not numCard.isdigit():
        return False
    card = list(str(numCard))
    last_number = card[len(card) - 1]
    card.pop()
    card.reverse()

    for index, item in enumerate(card):
        if index % 2 == 0:
            card[index] = int(item) * 2

    for index, item in enumerate(card):
        if int(item) > 9:
            card[index] = str(int(item) - 9)

    sum_card = 0
    for num in card:
        sum_card += int(num)

    total_sum = sum_card + int(last_number)
    if total_sum % 10 == 0:
        return True
    else:
        return False

def main():
    month = input("Entrez le mois d'expiration : ")
    year = input("Entrez l'année d'expiration : ")
    cvv = input("Entrez votre CVV (au dos de la carte) : ")
    number = input("Entrez votre numéro de carte (16 Chiffres) : ")

    if verifyCardNumber(number) and verifyCVV(cvv) and verifyDate(month, year):
        print("Votre carte est valide.")
    else:
        print("Votre carte n'est pas valide.")

if __name__ == "__main__":
    main()