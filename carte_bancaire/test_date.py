import unittest
import functions


class TestCardDate(unittest.TestCase):
    
    # Check for any input if it's not empty
    def test_inputIsNotEmpty(self):
        self.assertFalse(functions.inputIsNotEmpty(''))

    # Check if the expiration year is not more 4 years after today (Consider today : 2021)
    def test_dateYearTooLong(self):
        self.assertFalse(functions.verifyDate("09","34"))
        self.assertFalse(functions.verifyDate("09","27"))

    # Check the validity of the card expiration date
    def test_dateNotExpired(self):
        self.assertFalse(functions.verifyDate("08", "01"))
        self.assertTrue(functions.verifyDate("08", "22"))

    # Check if the month number is between 01-12
    def test_dateMonthIsValid(self):
        self.assertFalse(functions.verifyDate("13", "22"))
        self.assertTrue(functions.verifyDate("12", "22"))

    # Check if the date is only numbers
    def test_dateIsNumbers(self):
        self.assertFalse(functions.verifyDate(" *", "22"))
        self.assertFalse(functions.verifyDate("09", "&^"))
    
    # Check if both inputs are exactly two chars
    def test_dateAreOnlyTwoChars(self):
        self.assertFalse(functions.verifyDate("123", "22"))
        self.assertFalse(functions.verifyDate("", "22"))


if __name__ == '__main__':
    unittest.main()
