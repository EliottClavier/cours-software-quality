#!/usr/bin/env python3

def sentenceType(char):
    if char == ".":
        return "Affirmatif / déclaratif"
    elif char == "!":
        return "Exclamatif"
    elif char == "?":
        return "Interrogatif"
    else:
        return "Non reconnu"


def main():
    user_string = input("Veuillez entrer une phrase : ")
    array = user_string.split()

    print("Phrase entrée : {}\nType de la phrase : {}\nPhrase découpée en tableau : {}"
          .format(user_string, sentenceType(user_string[-1]), array))


if __name__ == '__main__':
    main()
