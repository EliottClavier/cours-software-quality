#!/usr/bin/env python3

import os.path

# Tuple des opérateurs possibles
OPERATORS = ('+', '-', '*', '**', '/', '//', '%')
# Enregistrement du nom du fichier où sont ajoutés les résultats pour éviter de le renseigner plusieurs fois
# si plusieurs calculs s'enchainent
FILE_NAME = ''


# Fonction permettant de mettre fin au programme lorsque l'utilisateur tape !q
def terminate(string):
    if string == '!q':
        import sys
        sys.exit()
    return string


# Récupère une valeur entrée par l'utilisateur et vérifie s'il s'agit d'un nombre
def getNumber():
    number = terminate(input("Veuillez entrer un nombre : "))
    if not number.isdigit():
        print("Valeur invalide.")
        return getNumber()
    return number


# Récupère une valeur entrée par l'utilisateur et vérifie s'il s'agit d'un opérateur de la liste OPERATORS
def getOperator():
    operator = terminate(input("Choisir un opérateur ({}) : ".format(', '.join(OPERATORS))))
    if operator not in OPERATORS:
        print("Opérateur invalide.")
        return getOperator()
    return operator


# Réalise l'opération avec les nombres et l'opérateur donné
def calculate(first_number, operator, second_number):
    try:
        result = str(eval(first_number + operator + second_number))
        return [result, [first_number, operator, second_number, '=', result, '\n']]
    # Dans le cas d'une division par zéro, on fait repartir l'opération du début
    except ZeroDivisionError:
        print("Division par zéro impossible !")
        return main()


# Sauvegarde les résultats dans un fichier externe nommé par l'utilisateur
def saveResults(result):
    global FILE_NAME
    # Demande de nom de fichier
    if not FILE_NAME != '':
        FILE_NAME = input("Veuillez entrer le nom du fichier de sauvegarde : ")
    # Si le fichier n'existe pas, on le crée
    if not os.path.isfile("{}.txt".format(FILE_NAME)):
        f = open("{}.txt".format(FILE_NAME), "w+")
    # Sinon, on l'ouvre pour y ajouter du contenu à la suite
    else:
        f = open("{}.txt".format(FILE_NAME), "a+")

    result_string = ' '.join(result[1])
    # On affiche le résultat
    print("Résultat : {} ".format(result_string))
    # On écrit le résultat dans le fichier
    f.write(result_string)
    f.close()

    # On demande à l'utilisateur si'il veut soumettre le résultat obtenu à une opération
    continue_calculation = terminate(input("Continuer le calcul depuis le résultat obtenu (O/N) ? "))
    if continue_calculation.upper() == 'O':
        # Si oui, la première valeur dans l'opération est automatiquement le résultat précedent
        main(result[0])
    else:
        # Sinon, on repart avec une nouvelle opération
        main()


def main(first_number=None):
    print("\nTapez !q pour arrêter le programme à tout moment.")
    # Si l'opération ne se fait pas depuis un résultat précédent, le premier nombre est demandé à l'utilisateur
    if not first_number:
        first_number = getNumber()
    else:
        print("Résultat précédent : {}".format(first_number))
    # Demande de l'operateur
    operator = getOperator()
    # Demande du deuxième nombre de l'opération
    second_number = getNumber()
    # On envoie le résultat du calcul à la fonction qui sauvegarde les résultats dans un fichier
    saveResults(calculate(first_number, operator, second_number))


if __name__ == '__main__':
    main()
