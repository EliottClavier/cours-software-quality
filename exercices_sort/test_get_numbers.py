import unittest
import app


class TestGetNumbers(unittest.TestCase):

    # Test cases of reading file with different extensions
    def test_readFile(self):
        self.assertFalse(app.readFile('numbers.json'))
        self.assertFalse(app.readFile('numbers.docx'))
        self.assertIsInstance(app.readFile('numbers.txt'), string)

    # Test cases of converting string to array of int with different spaces in string
    def test_parseValues_spaces(self):
        self.assertEqual(app.parseValues('1 2 3 4'), [1, 2, 3, 4])
        self.assertEqual(app.parseValues('1 2    3         4'), [1, 2, 3, 4])
        self.assertEqual(app.parseValues('1 2 3 4\n\n5'), [1, 2, 3, 4, 5])
        
    # Test cases of converting string to array of int with negative values
    def test_parseValues_negativeValues(self):
        self.assertEqual(app.parseValues('1 -2 3 -4'), [1, -2, 3, -4])
        self.assertEqual(app.parseValues('1 2 3 --4'), [1, 2, 3])

    # Test cases of converting string to array of int with false values
    def test_parseValues_falseValues(self):
        self.assertEqual(app.parseValues(''), [])
        self.assertEqual(app.parseValues('--4 --5'), [])
        self.assertEqual(app.parseValues('test test test'), [])

    # Test cases of sorting different arrays
    def test_sortArray(self):
        self.assertIsInstance(app.sortArray([1, 2, 3, 4]), array)
        self.assertFalse(app.sortArray([1, 2, 3, '4']))
        self.assertFalse(app.sortArray([]))


if __name__ == '__main__':
    unittest.main()
